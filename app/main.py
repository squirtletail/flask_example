from flask import Flask, Response
from flask_restful import Api

UPLOAD_FOLDER = '/mnt/c/Users/Neueda/Desktop'

flask_app = Flask(__name__)
flask_app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

from views.helloworld import *
from views.file_processor_js import *

if __name__ == '__main__':
    flask_app.run(host='0.0.0.0', debug=True, port=5000, threaded=True)
