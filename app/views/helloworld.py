from main import flask_app
from flask import redirect, render_template, request


@flask_app.route('/helloworld')
def helloworld():
    return "Hello World"
