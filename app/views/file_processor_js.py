from main import flask_app
from flask import jsonify, redirect, render_template, request
from werkzeug.utils import secure_filename
import os
import pandas as pd

ALLOWED_EXTENSIONS = set(['csv', 'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])


@flask_app.route('/upload', methods=['GET'])
def file_uploader():
    return render_template('file_uploader_js.html')


@flask_app.route('/uploadajax', methods=['POST'])
def file_upload_handler():
    data = {}

    if 'file' not in request.files:
        return redirect(request.url)

    file = request.files['file']

    if file.filename == '':
        return redirect(request.url)
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(flask_app.config['UPLOAD_FOLDER'],
                               filename))
        results = transform_file(filename)
        data['results'] = results.values.tolist()
        data['columns'] = results.columns.tolist()

    return jsonify(data)


def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def transform_file(filename):
    df = pd.read_csv(os.path.join(flask_app.config['UPLOAD_FOLDER'],
                                  filename))

    df.columns = ['INDEX', 'SYMBOL', 'EXCHANGE', 'TAPE']
    return df
