from main import flask_app
from flask import flash, redirect, render_template, request,\
                  url_for, send_from_directory
from werkzeug.utils import secure_filename
import os
import pandas as pd

ALLOWED_EXTENSIONS = set(['csv', 'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])


def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@flask_app.route('/upload', methods=['GET', 'POST'])
def file_uploader():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)

        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(flask_app.config['UPLOAD_FOLDER'],
                                   filename))
            results = transform_file(filename)

            ''' return redirect(url_for('uploaded_file',
                                    filename=filename))'''
            return render_template('uploaded_file.html',
                                   filename=filename,
                                   results=results)
    return render_template('file_uploader.html')


@flask_app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(flask_app.config['UPLOAD_FOLDER'],
                               filename)


def transform_file(filename):
    df = pd.read_csv(os.path.join(flask_app.config['UPLOAD_FOLDER'],
                                  filename))

    df.columns = ['INDEX', 'SYMBOL', 'EXCHANGE', 'TAPE']
    return df
