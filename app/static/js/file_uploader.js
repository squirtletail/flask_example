$(document).ready(function () {
    renderPage();
});

function renderPage() {
    $("body").on("click", "#submit-btn", function(e) {
        //e.preventDefault();
 
        var hideClass = "hide-block"
        $("#search-results").addClass(hideClass);
        var formData = new FormData($("#file-upload-form")[0]);

        csrfToken = $("csrf_token").val();

        $.ajax({
          url: '/uploadajax',
          type: 'POST',
          data: formData,
          contentType: false,
          processData: false,
          dataType: 'json',
          beforeSend: function( xhr ){
            xhr.setRequestHeader("X-CSRFToken", csrfToken)
          }
        }).done(function(data) {
            $("#search-results").removeClass(hideClass);
            console.log("done");
            renderTable('#search-results-text', data.results, data.columns);
            $("#search-results-text-block").scrollTop(0)
        }).fail(function(data){
            console.log("upload error")
        });
    });
}

function renderTable(element, dataset, columns) {
    if ($.fn.dataTable.isDataTable( element ) ) {
        $(element).DataTable().clear().destroy().draw();
        $(element + ' tbody').empty();
        $(element + ' thead').empty();
    };
    $(element).DataTable( {
        destroy: true,
        data: dataset,
        "scrollX": true, 
        columns : [
            { data: 0, title: columns[0] },
            { data: 1, title: columns[1] },
            { data: 2, title: columns[2] },
            { data: 3, title: columns[3] }
        ]
    });
}
